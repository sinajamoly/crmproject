import React, { useMemo } from 'react';
import { Card, Button, Image } from "antd";
import { Link } from 'react-router-dom';
import { ReadOutlined } from '@ant-design/icons';
import "Presentation/PostCard.css"

export default function Post ({ post : { id, title, body } }) {
    const MAX_LENGHT_OF_TITLE = 15;
    const MAX_LENGHT_OF_BODY = 100;
    const subBody = useMemo(() => {
        return body.length > MAX_LENGHT_OF_BODY ? body.substring(0, MAX_LENGHT_OF_BODY) + " . . ." : body;
    }, [body]);

    const subTitle = useMemo(() => {
       return title.length > MAX_LENGHT_OF_TITLE ? title.substring(0, MAX_LENGHT_OF_TITLE) + " . . ." : title
    }, [title]);

    return (
        <Card
            className="post-card-container"
            cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
        >
            <h2 className=''>{ subTitle }</h2>
            <p className=''>{ subBody }</p>
            <div className="">
                <Button type="primary" shape="circle">
                    <Link to={`post/${id}`}>
                        <ReadOutlined />
                    </Link>
                </Button>
            </div>
        </Card>
    )
}