import React from 'react';
import RichTextEditor from 'react-rte';
import 'Presentation/TextEditor/TextEditor.css';

 export default function TextEditor (props) {
     return (
         <div className="text-editor">
             <RichTextEditor
                 value={props.value}
                 onChange={props.onChange}
             />
         </div>
    );
}