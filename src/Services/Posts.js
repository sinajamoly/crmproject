import axios from 'axios';

export function getPosts()
{
    return axios.get('https://jsonplaceholder.typicode.com/posts')
}

export function getPost(id)
{
    return axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
}

