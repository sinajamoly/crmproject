import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
const { Header, Content, Footer } = Layout;

export default function ViewerLayout(props) {
    return (
        <Layout className="layout">
            <Content style={{ padding: '0 50px'}}>
                <div className="site-layout-content">
                    {props.children}
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>This is Designed by Sina Jamoly</Footer>
        </Layout>
    );
}