import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
const { Header, Content, Footer } = Layout;


export default function AdminLayout() {
    return (
        <Layout className="layout">
            <Content style={{ padding: '0 50px'}}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                </Breadcrumb>
                <div className="site-layout-content">
                    Admin page
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>This is Designed by Sina Jamoly</Footer>
        </Layout>
    );
}