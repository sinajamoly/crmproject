import React from 'react';
import { Menu, Layout } from "antd";
import { Link } from "react-router-dom";
const { Header } = Layout;

export default function HeaderLayout(props) {
    return (
        <Header>
            <div className="logo" />
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                <Menu.Item key="1">
                    <Link to='/view'>
                        Post
                    </Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <Link to='/admin'>
                        Admin
                    </Link>
                </Menu.Item>
            </Menu>
        </Header>
    );
}