import React, { useState, useEffect } from 'react';
import { Row, Col, Divider, Pagination } from 'antd';
import { getPosts } from "Services/Posts";
import Post from "../Presentation/Post";

export default function Posts() {
    const [ posts, setPosts ] = useState([]);
    const [ pageNumber, setPageNumber] = useState(1);
    const [ numberOfPostPerPage, setNumberOfPostPerPage] = useState(10);

    const onPageChange = (page, pageSize) => {
        if (page === 0) {
            setPageNumber(1);
        } else {
            setPageNumber(page);
        }

        setNumberOfPostPerPage(pageSize);
    };

    const paginationCalculator = (pageNumber, numberPerPage) => {
        return [
            numberPerPage * (pageNumber - 1),
            numberPerPage * (pageNumber)
        ];
    };

    useEffect(() => {
        getPosts().then((posts) => {
            setPosts(posts.data);
        }).catch((() => {

        }));
    }, []);

    const pagination = paginationCalculator(pageNumber, numberOfPostPerPage);

    return (
        <>
            <Divider/>
            <Row >
                {
                    posts.length > 0 &&
                    posts.slice(pagination[0], pagination[1]).map((post) => {
                        return (
                            <Col span={8} style={{ paddingBottom: 20 }} key={post.id}>
                                <Post post={post}/>
                            </Col>
                        );
                    })
                }
            </Row>
            <Divider/>
            <Pagination
                defaultCurrent={pageNumber}
                total={posts.length}
                onChange={onPageChange}
            />
            <Divider/>
        </>
    );
}