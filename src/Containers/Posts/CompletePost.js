import React, { useEffect, useRef, useState } from 'react';
import {getPost} from "../../Services/Posts";

export default function CompletePost ({ match }) {
    const postRef = useRef(match.params.id);

    const [ title, setTitle ] = useState(null);
    const [ body, setBody ] = useState(null);
    const [ loading, setLoading ] = useState(true);

    useEffect(() => {
        getPost(postRef.current).then((response) => {
            console.log(response);
            setTitle(response.data.title);
            setBody(response.data.body);
            setLoading(false);
        });
    }, [postRef.current]);

    const renderPost = () => {
        return (
            <div>
                <h1>{title}</h1>
                <p>{body}</p>
            </div>
        );
    };

    return (
        <>
            {!loading ? renderPost() : <h1>Loading</h1>}
        </>
    )
}