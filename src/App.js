import React from 'react';
import 'antd/dist/antd.css';
import Posts from "Containers/Posts";
import ViewerLayout from "Containers/ViewerLayout/ViewerLayout";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AdminLayout from "./Containers/AdminLayout/AdminLayout";
import HeaderLayout from "./Containers/Header/HeaderLayout";
import CompletePost from "./Containers/Posts/CompletePost";
import CreatePost from "./Containers/CreatePost/CreatePost";

function App() {
  return (
    <>
        <BrowserRouter>
            <HeaderLayout/>
            <Switch>
                <Route path='/view'>
                    <ViewerLayout>
                        <Posts/>
                    </ViewerLayout>
                </Route>
                <Route path='/admin'>
                    <AdminLayout/>
                </Route>
                <Route path="/post/create" component={CreatePost} exact/>
                <Route path="/post/:id" component={CompletePost}/>
            </Switch>
        </BrowserRouter>
    </>
  );
}

export default App;
